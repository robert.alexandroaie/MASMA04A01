/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dc.ac.masma.app;

import com.sun.xml.internal.ws.util.StringUtils;
import dc.ac.masma.app.helper.JadeHelper;
import dc.ac.masma.model.utils.ContextConstants;
import jade.core.Profile;
import jade.core.Runtime;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

/**
 *
 * @author lab
 */
public class L4A1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        AgentContainer mc = JadeHelper.createContainer(ContextConstants.CONTAINER_1, true, ContextConstants.LOCALHOST, "", "1090");
        AgentContainer c2 = JadeHelper.createContainer(ContextConstants.CONTAINER_2, false, ContextConstants.LOCALHOST, "", "1091");

        try {
            mc.start();
            c2.start();
        } catch (ControllerException e) {
        }

        AgentController displayAgentController = JadeHelper.createAgent(mc, ContextConstants.DISPLAY_AGENT_1, ContextConstants.AGENTS_PACKAGE+"DisplayAgent", null);
        AgentController readingFileMobileAgentController = JadeHelper.createAgent(mc, ContextConstants.READING_FILE_MOBILE_AGENT_1, ContextConstants.AGENTS_PACKAGE+"ReadingFileMobileAgent", null);
        AgentController filePathInfoAgentController = JadeHelper.createAgent(c2, ContextConstants.FILE_PATH_INFO_AGENT_1, ContextConstants.AGENTS_PACKAGE+"FileInfoAgent", null);

        try {
            displayAgentController.start();
            readingFileMobileAgentController.start();
            filePathInfoAgentController.start();
        } catch (StaleProxyException e) {
        }

    }

}
