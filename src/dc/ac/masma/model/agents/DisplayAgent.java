package dc.ac.masma.model.agents;

import dc.ac.masma.model.behaviours.ReceiveFileContentBehaviour;
import jade.core.Agent;


public class DisplayAgent extends Agent implements TrainedBehaviour{
    
    @Override
    public void setup()
    {

        registerBehaviours();
    }
    
    @Override
    public void takeDown()
    {
        
    }

    @Override
    public void registerBehaviours() {

        addBehaviour(new ReceiveFileContentBehaviour());
    }
}
