
package dc.ac.masma.model.agents;

import dc.ac.masma.model.FileInfo;
import dc.ac.masma.model.behaviours.AskFileInfoBehaviour;
import dc.ac.masma.model.behaviours.MovingBehaviour;
import dc.ac.masma.model.behaviours.ReceiveFileInfoBehaviour;
import dc.ac.masma.model.behaviours.ReportFileContentBehaviour;
import dc.ac.masma.model.utils.ContextConstants;
import jade.core.*;

public class ReadFileMobileAgent extends Agent implements TrainedBehaviour{
    
    private FileInfo fileInfo;

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }
    
    @Override
    public void setup()
    {
        System.out.println("Agent " + getAID().getName() + " started...");
    }
    
    @Override
    public void beforeMove()
    {
        System.out.println("Agent " + getAID().getName() + " began moving...");
    }
    
    @Override
    public void afterMove()
    {
        System.out.println("Agent " + getAID().getName() + " finished moving...");
    }

    @Override
    public void registerBehaviours() {
       
        addBehaviour(new MovingBehaviour(ContextConstants.CONTAINER_2));
        addBehaviour(new AskFileInfoBehaviour());
        addBehaviour(new ReceiveFileInfoBehaviour());
        addBehaviour(new ReportFileContentBehaviour());
    }
}
