/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dc.ac.masma.model.behaviours;

import dc.ac.masma.model.agents.ReadFileMobileAgent;
import dc.ac.masma.model.utils.ContextConstants;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.introspection.ACLMessage;

/**
 *
 * @author lab
 */
public class AskFileInfoBehaviour extends OneShotBehaviour{

    private ReadFileMobileAgent myAgent;
    
    @Override
    public void action() {
        
        ACLMessage message = new ACLMessage();
        AID receiverAID = new AID(ContextConstants.FILE_PATH_INFO_AGENT_1, AID.ISLOCALNAME);
        message.addReceiver(receiverAID);
        message.setContent("Hello");
        myAgent.send(message);
        
    }
    
    
}
