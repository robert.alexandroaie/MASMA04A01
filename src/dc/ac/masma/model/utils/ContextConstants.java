/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dc.ac.masma.model.utils;

/**
 *
 * @author lab
 */
public class ContextConstants {
    
    // CONTAINERS
    public final static String CONTAINER_1 = "container1";
    public final static String CONTAINER_2 = "container2";
    public final static String LOCALHOST = "localhost";
    
    // AGENTS
    public final static String READING_FILE_MOBILE_AGENT_1="ReadingFileMobileAgent1";
    public final static String DISPLAY_AGENT_1="DisplayAgent1";
    public final static String FILE_PATH_INFO_AGENT_1="FilePathAgent1";
    
    public final static String MODEL_PACKAGE = "dc.ac.masma.model";
    public final static String AGENTS_PACKAGE = MODEL_PACKAGE + ".agents";
}
